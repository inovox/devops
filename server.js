const express = require('express')
const app = express()
const port = process.env.PORT || 3000
 
app.get('/', (req, res) => {
 /*res.send('Servidor backend em execucao.')*/
 resposta = {
                mensagem: "servidor em execucao."
            }
 res.status(200).json(resposta) 
})
 
app.listen(port, () => {
 console.log(`Servidor inicializado na porta :${port}`)
})
