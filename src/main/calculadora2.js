function funcao(i, j, k, l) {
   // start
   var result;
   if(i % 2 == 0) {
       // iEven
       result = i + j + k + l;
   }
   else {
       // iOdd
       if(j % 2 == 0) {
           // jEven
           result = i * j * k * l;
       }
       else {
           // jOdd
           result = i - j - k - l;
       }
   }
   return result;
   // end
}
 
module.exports = funcao;
